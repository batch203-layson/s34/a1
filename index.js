/*
// Load the expressjs module into our application and save it in a variable called express
*/

//syntax: require("package");
const express = require("express");

//localhost port number 
const port = 4000;

// app is our server
//  create an application that uses express and stores it as app
const app = express();

// middleware
// streaming of data and automatically parse the incoming JSON from our req.body
app.use(express.json());

// mock data
let users = [
	{
		username: "Stark3000",
		email: "starkindustries@mail.com",
		password: "notPeterParker",
	},
	{
		username: "ThorThunder",
		email: "loveAndThunder@mail.com",
		password: "iLoveStormBreaker",
	}
	
];

let items = [
	{
		name: "Mjolnir",
		price: 50000,
		isActive: true
	},
	{
		name: "Vibranium Shield",
		price: 70000,
		isActive: true
	}
	
];


/*
// Express has methods to use as routes corresponding to HTTP methods

syntax:
app.method(<endpoint>, <function for req and res>)

*/

app.get('/', (req, res) => {
	// res.send() actually combines writeHead() and end()
	res.send('Hello from my first expressJS API');
	// res.status(200).send("message");
});



app.get('/greeting', (req, res) => {
	// res.send() actually combines writeHead() and end()
	res.send('Hello from Batch203-Layson');
	// res.status(200).send("message");
});



// retrieval of the users in mock database

app.get('/users', (req, res) =>{
	// res.send already stringifies for you
	res.send(users);
});



// Adding of new user
app.post('/users', (req, res) =>{
	
	console.log(req.body); //result: {} empty object

	//simulate creating a new user document
	let newUser = {
		username: req.body.username,
		email: req.body.email,
		password: req.body.password
	}

	// adding newUser to the existing array
	users.push(newUser);
	console.log(users);

	// send the updated users array in the client
	res.send(users);

});

/*
DELETE METHOD
*/
app.delete('/users', (req, res)=>{
	users.pop();
	res.send(users);
});

/*
PUT METHOD
update user's password
*/
app.put('/users/:index', (req, res)=>{
	console.log(req.body); //updated password
	//result: {}

	//an object that contains the value of URL params
	console.log(req.params); // refer to the wildcard in the endpoint
	// result: {index:0}

	// parseInt the value of the number coming from req.params
	// ['0'] turns to [0]
	let index = parseInt(req.params.index);

	// users[0].password
		// "notPeterParker" = "tonyStark"
		// the update will be coming from the request body
	users[index].password = req.body.password;
	// send the updated user
	res.send(users[index]);

});


//>> Create a new route to get and send items array in the client (GET ALL ITEMS)

app.get('/items', (req, res) =>{
	// res.send already stringifies for you
	res.send(items);
});


/*

    >> Create a new route to create and add a new item object in the items array (CREATE ITEM)
        >> send the updated items array in the client
        >> check the post method route for our users for reference
*/


app.post('/items', (req, res) =>{
	
	console.log(req.body); //result: {} empty object

	//simulate creating a new user document
	let newItem = {
		name: req.body.name,
		price: req.body.price,
		isActive: req.body.isActive
	}

	// adding newUser to the existing array
	items.push(newItem);
	console.log(items);

	// send the updated users array in the client
	res.send(items);
});



/*

    >> Create a new route which can update the price of a single item in the array (UPDATE ITEM)
        >> pass the index number of the item that you want to update in the request params
        >> add the price update in the request body
        >> reassign the new price from our request body
        >> send the updated item in the client
*/


app.put('/items/:index', (req, res)=>{
	console.log(req.body); 

	
	console.log(req.params); 
	

	let index = parseInt(req.params.index);

	items[index].price = req.body.price;
	
	res.send(items[index]);

});

// port listener
app.listen(port, ()=> console.log(`Server is running at port ${port}`))




/*
Activity : 30 mins

    Endpoint: /items

    >> Create a new route to get and send items array in the client (GET ALL ITEMS)

    >> Create a new route to create and add a new item object in the items array (CREATE ITEM)
        >> send the updated items array in the client
        >> check the post method route for our users for reference

    >> Create a new route which can update the price of a single item in the array (UPDATE ITEM)
        >> pass the index number of the item that you want to update in the request params
        >> add the price update in the request body
        >> reassign the new price from our request body
        >> send the updated item in the client

    >> Create a new collection in Postman called s34-activity
    >> Save the Postman collection in your s34 folder
*/